package de.edrup.confluence.plugins.simpleanonymizer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Version;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.people.User;
import com.atlassian.confluence.api.model.relations.RelationInstance;
import com.atlassian.confluence.api.model.search.ContentSearchResult;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.api.model.search.SearchOptions;
import com.atlassian.confluence.api.model.search.SearchPageResponse;
import com.atlassian.confluence.api.model.search.SearchResult;
import com.atlassian.confluence.api.model.search.SpaceSearchResult;
import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.api.service.relations.RelationService;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostUpdateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageUpdateTrigger;
import com.atlassian.confluence.pages.TrashManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.user.search.query.TermQuery;
import com.atlassian.user.search.query.TwoTermBooleanQuery;
import com.atlassian.confluence.setup.settings.GlobalSettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceStatus;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.core.bean.EntityObject;
import com.atlassian.event.api.EventPublisher;
import org.json.JSONArray;
import org.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.MarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.search.query.BooleanQuery;
import com.atlassian.user.search.query.FullNameTermQuery;
import com.atlassian.user.search.query.GroupNameTermQuery;
import com.atlassian.user.search.query.UserNameTermQuery;

import static com.atlassian.confluence.api.model.relations.CollaboratorRelationDescriptor.COLLABORATOR;


@Named
public class SimpleAnonymizerHelper {
	
	private final LikeManager likeMan;
	private final TransactionTemplate transactionTemplate;
    private final UserAccessor userAcc;
    private final NotificationManager notificationMan;
	private final CQLSearchService searchService;
	private final ContentEntityManager contentEntityMan;
	private final GlobalSettingsManager settingsMan;
	private final I18nResolver i18n;
	private final SpaceManager spaceMan;
	private final RelationService relationService;
	private final PluginLicenseManager licenseMan;
	private final EventPublisher eventPublisher;
	private final MarshallingRequestFactory <?> requestFactory;
	private final TrashManager trashMan;
	private final AccessModeService accessModeService;
	private final GroupManager groupMan;
	private final PermissionManager permissionMan;
	private final ConfluenceIndexer confluenceIndexer;
	private final VelocityHelperService velocityHelperService;

	private static final Logger log = LoggerFactory.getLogger(SimpleAnonymizerHelper.class);
	
	private final int SEARCH_PAGE_SIZE = 200;
	private final int BATCH_SIZE = 20;
	private final boolean CHECK_LICENSE = true;
	private final int MAX_EVAL_LICENSE = 3000;
	
    private static ExecutorService pool = Executors.newFixedThreadPool(4);    
    
    private HashMap<String, SimpleAnonymizerContext> contextMap;
    private HashMap<String, Boolean> cancelRequest;
    private Cache<String, Boolean> validTokens;

	
	@Inject
	public SimpleAnonymizerHelper(@ComponentImport LikeManager likeMan, @ComponentImport TransactionTemplate transactionTemplate, @ComponentImport UserAccessor userAcc,
		@ComponentImport NotificationManager notificationMan, @ComponentImport CQLSearchService searchService, @ComponentImport("contentEntityManager") ContentEntityManager contentEntityMan,
		@ComponentImport GlobalSettingsManager settingsMan, @ComponentImport I18nResolver i18n, @ComponentImport SpaceManager spaceMan, @ComponentImport RelationService relationService,
		@ComponentImport ConfluenceIndexer confluenceIndexer, @ComponentImport PluginLicenseManager licenseMan, @ComponentImport EventPublisher eventPublisher,
		@ComponentImport MarshallingRequestFactory <?> requestFactory, @ComponentImport TrashManager trashMan, @ComponentImport PermissionManager permissionMan,
		@ComponentImport AccessModeService accessModeService, @ComponentImport GroupManager groupMan, @ComponentImport CacheManager cacheMan,
		@ComponentImport VelocityHelperService velocityHelperService) {
		this.likeMan = likeMan;
		this.transactionTemplate = transactionTemplate;
		this.userAcc = userAcc;
		this.notificationMan = notificationMan;
		this.searchService = searchService;
		this.contentEntityMan = contentEntityMan;
		this.settingsMan = settingsMan;
		this.i18n = i18n;
		this.spaceMan = spaceMan;
		this.relationService = relationService;
		this.licenseMan = licenseMan;
		this.eventPublisher = eventPublisher;
		this.requestFactory = requestFactory;
		this.trashMan = trashMan;
		this.accessModeService = accessModeService;
		this.groupMan = groupMan;	
		this.permissionMan = permissionMan;
		this.confluenceIndexer = confluenceIndexer;
		this.velocityHelperService = velocityHelperService;
		
		contextMap = new HashMap<String, SimpleAnonymizerContext>();
		cancelRequest = new HashMap<String, Boolean>();
		validTokens = cacheMan.getCache("Simple Anonymizer token cache", null,
			new CacheSettingsBuilder().remote().expireAfterWrite(6, TimeUnit.HOURS).build());
	}
	
	
	// anonymize contributions et al of the user
	public String anonymize(SimpleAnonymizerContext anonymizeContext) {
		
		String requestId = getSaltString();
		anonymizeContext.checkLicense = CHECK_LICENSE;
		anonymizeContext.requestId = requestId;
		contextMap.put(requestId, anonymizeContext);
		
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		CompletableFuture.supplyAsync(() -> doAnonymize(anonymizeContext, user), pool);
		
		return new JSONObject().put("requestId", requestId).toString();
	}
	
	
	// get the rendered result back
	public String getRenderedResult(String requestId) {
		boolean finished = (contextMap.get(requestId) != null) ? contextMap.get(requestId).finished : false;

		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("results", (contextMap.get(requestId) != null) ? contextMap.get(requestId).results : new ArrayList<JSONObject>());
		
		if(finished) {
			contextMap.remove(requestId);
		}
		
		return velocityHelperService.getRenderedTemplate("/templates/simple-anonymize-results.vm", context);
	}
	
	
	// get the result back in JSON format
	public String getJSONResult(String requestId) {
		boolean finished =  (contextMap.get(requestId) != null) ? contextMap.get(requestId).finished : false;
		
		JSONObject result = new JSONObject();
		result.put("finished", finished);
		result.put("results",   (contextMap.get(requestId) != null) ? contextMap.get(requestId).results : new ArrayList<JSONObject>());
		
		if(finished) {
			contextMap.remove(requestId);
		}
		
		return result.toString();
	}
	
	
	// get the status back
	public String getStatus(String requestId) {
		return new JSONObject().put("finished", (contextMap.get(requestId) != null) ? contextMap.get(requestId).finished : false).put("progress", contextMap.get(requestId).progress).toString();
	}
	
	
	// get all users and groups matching the query (we need this for the UI as Confluence's own functions only return active users)
	public String getUsers(String query) {
		JSONArray foundUsersAndGroups = new JSONArray();
		try {
			TermQuery<com.atlassian.user.User> t1 = new FullNameTermQuery(query, FullNameTermQuery.SUBSTRING_CONTAINS);
			TermQuery<com.atlassian.user.User> t2 = new UserNameTermQuery(query, UserNameTermQuery.SUBSTRING_CONTAINS);
			List<com.atlassian.user.User> results = userAcc.findUsersAsList(new TwoTermBooleanQuery<com.atlassian.user.User>(t1, t2, BooleanQuery.OR));
			for(com.atlassian.user.User result : results) {
				JSONObject user = new JSONObject();
				user.put("nameForList", result.getFullName() + " (" + result.getName() + ")");
				user.put("name", result.getName());
				foundUsersAndGroups.put(user);
			}
			com.atlassian.user.search.SearchResult<Group> groupResults = userAcc.findGroups(new GroupNameTermQuery(query, GroupNameTermQuery.SUBSTRING_CONTAINS));
			Iterator<Group> groupIterator = groupResults.pager().iterator();
			while(groupIterator.hasNext()) {
				Group group = groupIterator.next();
				JSONObject groupForList = new JSONObject();
				groupForList.put("nameForList", group.getName() + " (" + i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.ui.group") + ")");
				groupForList.put("name", group.getName());
				foundUsersAndGroups.put(groupForList);
			}
		}
		catch(Exception e) {}
		return foundUsersAndGroups.toString();
	}
	
	
	// request the cancellation of an anonymization job
	public String cancelRequest(String requestId) {
		cancelRequest.put(requestId, true);
		return new JSONObject().put("success", true).toString();
	}
	

	// do the anonymization
	private boolean doAnonymize(SimpleAnonymizerContext anonymizeContext, ConfluenceUser user) {
		
		AuthenticatedUserThreadLocal.set(user);
		
		// license check
		boolean validLicense = licenseMan.getLicense().isDefined() ? licenseMan.getLicense().get().isValid() : false;
		if(!validLicense && anonymizeContext.execute && anonymizeContext.checkLicense) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.license"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		
		// read-only check
		if(accessModeService.isReadOnlyAccessModeEnabled() && anonymizeContext.execute) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.readonly"), "", false);
			return cleanUp(anonymizeContext, false);		
		}
		
		// check if provided name to anonymize is a user or group name
		boolean isUser = userAcc.getUserByName(anonymizeContext.userOrGroup) != null;
		boolean isGroup = false;
		try {
			isGroup = groupMan.getGroup(anonymizeContext.userOrGroup) != null;
		}
		catch(Exception e) {}
		if(!isUser && !isGroup) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.user"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		
		// check if the target user exists and is not deactivated
		if((userAcc.getUserByName(anonymizeContext.targetUserName) == null) || userAcc.isDeactivated(anonymizeContext.targetUserName)) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.targetUser"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		
		// check if a toDate is defined in case of a group anonymization
		if(anonymizeContext.toDateString.equals(anonymizeContext.MAX_DATE) && isGroup) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.groupMaxDate"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		
		// get the necessary dates
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			anonymizeContext.fromDate = format.parse(anonymizeContext.fromDateString);
			anonymizeContext.toDate = format.parse(anonymizeContext.toDateString);
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.date"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.date"), "", true);
		
		// compile the list of users to anonymize
		ArrayList<String> userNames = new ArrayList<String>();
		if(isUser) {
			userNames.add(anonymizeContext.userOrGroup);
		}
		if(isGroup) {
			try {
				userNames.addAll(userAcc.getMemberNamesAsList(groupMan.getGroup(anonymizeContext.userOrGroup)));
			}
			catch(Exception e) {}
		}

		// collect things to anonymize
		ArrayList<SimpleAnonymizerResult> results = new ArrayList<SimpleAnonymizerResult>();
		for(String userName : userNames) {
			if(anonymizeContext.contributions) { results.addAll(transactionTemplate.execute(() -> search("contributor = \"" + userName + "\" and created <= \"" + anonymizeContext.toDateString + "\" and lastmodified >= \"" + anonymizeContext.fromDateString + "\"",  userName, anonymizeContext))); }
			if(anonymizeContext.mentions) { results.addAll(transactionTemplate.execute(() -> search("mention = \"" + userName + "\" and created <= \"" + anonymizeContext.toDateString + "\" and lastmodified >= \"" + anonymizeContext.fromDateString + "\"",  userName, anonymizeContext))); }
			if(anonymizeContext.tasks) { results.addAll(searchTasks(userName, anonymizeContext)); }
						
			// continue collecting
			if(anonymizeContext.includeTrash) { results.addAll(getTrashedObjects(userName, anonymizeContext)); }
		}
		
		// paid license check
		boolean paidLicense = (validLicense && !licenseMan.getLicense().get().isEvaluation());
		if(!paidLicense && anonymizeContext.execute &&  anonymizeContext.checkLicense && (results.size() > MAX_EVAL_LICENSE)) {
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.license.paid"), "", false);
			return cleanUp(anonymizeContext, false);
		}
		
		// anonymize the found results
		anonymizeAllResults(anonymizeContext, results, 0.9f);
		
		// the simple staff - will not be executed when anonymizing a group
		if(isUser) {
			if(anonymizeContext.personalSpace) { transactionTemplate.execute(() -> removePersonalSpace(anonymizeContext)); }
			if(anonymizeContext.likes) { transactionTemplate.execute(() -> removeLikes(anonymizeContext)); }
			if(anonymizeContext.watches) { transactionTemplate.execute(() ->removeWatches(anonymizeContext)); }
		}
		
		return cleanUp(anonymizeContext, true);
	}
	
	
	// finish
	private boolean cleanUp(SimpleAnonymizerContext anonymizeContext, boolean result) {
		anonymizeContext.progress = 1.0f;
		anonymizeContext.finished = true;
		return result;
	}
	
	
	// remove all likes of the given user
	private boolean removeLikes(SimpleAnonymizerContext anonymizeContext) {
		boolean success = true;
		try {
			if(anonymizeContext.execute) {
				likeMan.removeAllLikesFor(userAcc.getUserByName(anonymizeContext.userOrGroup).getKey());
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
		}
		anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.likes"), "", success);
		
		return success;
	}
	
	
	// remove all watches of the given user
	private boolean removeWatches(SimpleAnonymizerContext anonymizeContext) {
		boolean success = true;
		try {
			if(anonymizeContext.execute) {
				notificationMan.removeAllNotificationsForUser(userAcc.getUserByName(anonymizeContext.userOrGroup));
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
		}
		anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.watches"), "", success);
		
		return success;
	}
	
	
	// remove personal space
	private boolean removePersonalSpace(SimpleAnonymizerContext anonymizeContext) {
		boolean success = true;
		if(spaceMan.getPersonalSpace(userAcc.getUserByName(anonymizeContext.userOrGroup)) != null) {
			try {
				if(anonymizeContext.execute) {
					spaceMan.removeSpace(spaceMan.getPersonalSpace(userAcc.getUserByName(anonymizeContext.userOrGroup)));
				}
			}
			catch(Exception e) {
				log.error(e.toString());
				log.debug(getStackTrace(e));
				success = false;
			}
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.personalSpace"), "", success);
		}
		
		return success;
	}
		
	
	// perform the requested search and apply the action on all results in all versions
	@SuppressWarnings("rawtypes")
	private ArrayList<SimpleAnonymizerResult> search(String cql, String userName, SimpleAnonymizerContext anonymizeContext) {
		
		ArrayList<SimpleAnonymizerResult> results = new ArrayList<SimpleAnonymizerResult>();
		
		// try to perform the requested search
		ArrayList<SearchResult> cqlResultsAsList = new ArrayList<SearchResult>();
		boolean success = true;
		try {
			SearchContext searchContext = SearchContext.builder().build();
			SearchOptions searchOptions = SearchOptions.builder()
				.searchContext(searchContext)
	            .excerptStrategy(SearchOptions.Excerpt.NONE)
	            .includeArchivedSpaces(anonymizeContext.includeArchivedSpaces)
	            .build();
			cql = anonymizeContext.limitToSpaces.isEmpty() ? cql : cql + " and space in (" + anonymizeContext.limitToSpaces + ")";
			boolean hasMore = true;
			int start = 0;
			while(hasMore) {
				SearchPageResponse<SearchResult> cqlResult = searchService.search(cql, searchOptions, new SimplePageRequest(start, SEARCH_PAGE_SIZE), new Expansion(""));
				cqlResultsAsList.addAll(cqlResult.getResults());
				hasMore = cqlResult.hasMore();
				start += SEARCH_PAGE_SIZE;
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;	
		}
		anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.search"), cql, cqlResultsAsList.size()), "", success);
		
		// loop through all results
		for(SearchResult singleCQLResult : cqlResultsAsList) {
			if(singleCQLResult instanceof ContentSearchResult) {
				results.add(new SimpleAnonymizerResult(((ContentSearchResult) singleCQLResult).getContent().getId().asLong(), userName, cql.contains("mention") ? SimpleAnonymizerResultType.MENTION : SimpleAnonymizerResultType.CONTRIBUTION_CONTENT));
			}
			if(singleCQLResult instanceof SpaceSearchResult) {
				results.add(new SimpleAnonymizerResult(((SpaceSearchResult) singleCQLResult).getSpace().getKey(), userName));
			}
		}
		
		return results;
	}
	
	
	// get all tasks of the user
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList<SimpleAnonymizerResult> searchTasks(String userName, SimpleAnonymizerContext anonymizeContext) {
		
		boolean success = true;
		ArrayList<SimpleAnonymizerResult> results = new ArrayList<SimpleAnonymizerResult>();
		
		try {
			String url = getBaseUrl() + "/rest/mywork/1/task?bypass=" + userName;
			Request request = requestFactory.createRequest(MethodType.GET, url);
			//request.addTrustedTokenAuthentication(new URI(getBaseUrl()).getHost());
		
			request.execute(new ResponseHandler() {
				@Override
				public void handle(Response response) throws ResponseException {
					
					// get all affected contentIds from the response
					JSONArray jsonResponse = new JSONArray(response.getResponseBodyAsString());
					ArrayList<Long> contentIds = new ArrayList<Long>();
					for(int n = 0; n < jsonResponse.length(); n++) {
						Long contentId = Long.parseLong(jsonResponse.getJSONObject(n).getJSONObject("metadata").getString("contentId"));
						if(!contentIds.contains(contentId)) {
							contentIds.add(contentId);
						}
					}
					
					// store them as results
					for(int n = 0; n < contentIds.size(); n++) {
						SimpleAnonymizerResult result = new SimpleAnonymizerResult(contentIds.get(n), userName, SimpleAnonymizerResultType.TASK);
						results.add(result);
					}
				}
			});
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
		}
		
		anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.tasks"), results.size(), userName), "", success);
		
		return results;
	}
	
	
	// get all trashed content objects
	private ArrayList<SimpleAnonymizerResult> getTrashedObjects(String userName, SimpleAnonymizerContext anonymizeContext) {
		
		boolean success = true;
		ArrayList<SimpleAnonymizerResult> results = new ArrayList<SimpleAnonymizerResult>();
		try {
			ArrayList<String> spaceKeys = new ArrayList<String>();
			if(anonymizeContext.limitToSpaces.isEmpty()) {
				spaceKeys.addAll(spaceMan.getAllSpaceKeys(SpaceStatus.CURRENT));
				if(anonymizeContext.includeArchivedSpaces) {
					spaceKeys.addAll(spaceMan.getAllSpaceKeys(SpaceStatus.ARCHIVED));
				}
			}
			else {
				spaceKeys.addAll(Arrays.asList(anonymizeContext.limitToSpaces.split(",")));
			}
			for(String spaceKey : spaceKeys) {
				List<ContentEntityObject> inTrash = trashMan.getTrashContents(spaceMan.getSpace(spaceKey), 0, 20000);
				for(ContentEntityObject ceo : inTrash) {
					SimpleAnonymizerResult result = new SimpleAnonymizerResult(ceo.getId(), userName, SimpleAnonymizerResultType.TRASH);
					results.add(result);
					if((ceo instanceof Page) || (ceo instanceof BlogPost)) {
						List<Comment> comments = ceo.getComments();
						for(Comment comment : comments) {
							result = new SimpleAnonymizerResult(comment.getId(), userName, SimpleAnonymizerResultType.TRASH);
							results.add(result);
						}
						List<Attachment> attachments = ceo.getAttachments();
						for(Attachment attachment : attachments) {
							result = new SimpleAnonymizerResult(attachment.getId(), userName, SimpleAnonymizerResultType.TRASH);
							results.add(result);
						}
					}
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
		}
		
		anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.trash"), results.size(), userName), "", success);
		
		return results;
	}
	
	
	// anonymize all results found
	private void anonymizeAllResults(SimpleAnonymizerContext anonymizeContext, ArrayList<SimpleAnonymizerResult> results, float progressEnd)
	{
		float progressInc = (progressEnd - anonymizeContext.progress) / (float) results.size();
		int start = 0;
		while(start < results.size()) {
			int end = (start + BATCH_SIZE < results.size()) ? start + BATCH_SIZE : results.size();
			anonymizeBatchInTransaction(anonymizeContext, results, start, end, progressInc);
			start += BATCH_SIZE;
			if(cancelRequest.containsKey(anonymizeContext.requestId)) {
				cancelRequest.remove(anonymizeContext.requestId);
				anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.cancelled"), "", false);
				return;
			}
		}
	}
	
	
	// anonymize a batch of results in a transaction
	private void anonymizeBatchInTransaction(SimpleAnonymizerContext anonymizeContext, ArrayList<SimpleAnonymizerResult> results, int start, int size, float progressInc) {
		try {
			transactionTemplate.execute(() -> anonymizeBatch(anonymizeContext, results, start, size, progressInc));
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			anonymizeContext.appendResult(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.batch.failed"), "", false);
		}
	}
	
	
	// anonymize a batch of results
	private boolean anonymizeBatch(SimpleAnonymizerContext anonymizeContext, ArrayList<SimpleAnonymizerResult> results, int start, int end, float progressInc) {
		
		// refresh when starting a batch
		anonymizeContext.currentUserName = "";
		anonymizeContext.targetUser = userAcc.getUserByName(anonymizeContext.targetUserName);
		
		// execute the batch
		for(int n = start; n < end; n++) {
			SimpleAnonymizerResult result = results.get(n);
			if(!anonymizeContext.currentUserName.equals(result.userName)) {
				anonymizeContext.currentUserName = result.userName;
				anonymizeContext.currentUser = userAcc.getUserByName(result.userName);
			}
			log.debug("Anonymizing {} in content id {}", results.get(n).type.name(), results.get(n).contentId);
			anonymizeResult(anonymizeContext, results.get(n));
			anonymizeContext.progress += progressInc;
		}
		
		return true;
	}
	
	
	// anonymize a single result
	private void anonymizeResult(SimpleAnonymizerContext anonymizeContext, SimpleAnonymizerResult result) {
		try {
			boolean reIndexNeeded = false;
			
			if(result.type == SimpleAnonymizerResultType.CONTRIBUTION_CONTENT) {
				ContentEntityObject ceo = contentEntityMan.getById(result.contentId);
				while(ceo != null) {
					reIndexNeeded = anonymizeContent(anonymizeContext, ceo);
					ceo = contentEntityMan.getPreviousVersion(ceo);
				}
			}
			if(result.type == SimpleAnonymizerResultType.CONTRIBUTION_SPACE) {
				anonymizeSpace(anonymizeContext, spaceMan.getSpace(result.spaceKey));
			}
			if(result.type == SimpleAnonymizerResultType.MENTION) {
				ContentEntityObject ceo = contentEntityMan.getById(result.contentId);
				while(ceo != null) {
					anonymizeMention(anonymizeContext, ceo);
					ceo = contentEntityMan.getPreviousVersion(ceo);
				}
			}
			if(result.type == SimpleAnonymizerResultType.TASK) {
				ContentEntityObject ceo = contentEntityMan.getById(result.contentId);
				while(ceo != null) {
					anonymizeTask(anonymizeContext, ceo, true);
					ceo = contentEntityMan.getPreviousVersion(ceo);
				}
			}
			if(result.type == SimpleAnonymizerResultType.TRASH) {
				ContentEntityObject ceo = contentEntityMan.getById(result.contentId);
				while(ceo != null) {
					if(anonymizeContext.contributions) { reIndexNeeded = anonymizeContent(anonymizeContext, ceo); }
					if(anonymizeContext.mentions && ((ceo instanceof Page) || (ceo instanceof BlogPost))) { anonymizeMention(anonymizeContext, ceo); }
					if(anonymizeContext.tasks && ((ceo instanceof Page) || (ceo instanceof BlogPost))) { anonymizeTask(anonymizeContext, ceo, false); }
					ceo = contentEntityMan.getPreviousVersion(ceo);
				}
			}
			
			if(reIndexNeeded) {
				confluenceIndexer.synchronous().reIndex(contentEntityMan.getById(result.contentId));
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.content.failed"), result.contentId),"", false);		
		}
	}
	
	
	// anonymize creator and/or last modifier of the given ContentEnitityObject
	private boolean anonymizeContent(SimpleAnonymizerContext anonymizeContext, ContentEntityObject ceo) throws Exception {
		
		boolean success = true;
		String doneString = "";
		ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
		
		try {
			
			// creator
			if(anonymizeContext.currentUser.equals(ceo.getCreator()) && isCreationInTimeFrameAndSpace(ceo, anonymizeContext)) {
				if(anonymizeContext.execute) {
					ceo.setCreator(anonymizeContext.targetUser);
				}
				doneString += "creator;";
			}
			
			// last modifier
			if(anonymizeContext.currentUser.equals(ceo.getLastModifier()) && isModificationInTimeFrameAndSpace(ceo, anonymizeContext)) {
				if(anonymizeContext.execute) {
					ceo.setLastModifier(anonymizeContext.targetUser);
				}
				doneString += "modifier;";
			}
			
			// relations
			if(permissionMan.hasPermission(currentUser, Permission.VIEW, ceo) && permissionMan.hasPermission(anonymizeContext.targetUser, Permission.VIEW, ceo))
			{
				boolean wasTrashed = ceo.isDeleted();
				if(wasTrashed) {
					ceo.restore();
				}
				
				Content content = Content.builder(ContentType.valueOf(ceo.getTypeEnum().getTypeName()), ceo.getId()).status(ceo.getContentStatusObject()).version(Version.builder().number(ceo.getVersion()).build()).build();

				if(relationService.isRelated(User.fromUserkey(anonymizeContext.currentUser.getKey()), COLLABORATOR, content) && isModificationInTimeFrameAndSpace(ceo, anonymizeContext)) {
					
					if(anonymizeContext.execute) {
						// find all collaborators
						PageResponse<User> collaborators = relationService.findSources(content, COLLABORATOR).fetchMany(new SimplePageRequest(0, 100), new Expansion(""));
						
						// remove all collaborator relations (interestingly this is allowed by the service)
						relationService.removeAllRelationsFromEntityWithType(COLLABORATOR, content);
						
						// add them again (replacing the user we want to anonymize by the targetUser)
						for(User collaborator : collaborators) {
							if(collaborator.optionalUserKey().orElseGet(null) != null) {
								User collaboratorToAdd = collaborator.optionalUserKey().orElseGet(null).equals(anonymizeContext.currentUser.getKey()) ? User.fromUserkey(anonymizeContext.targetUser.getKey()) : collaborator;
								AuthenticatedUserThreadLocal.set(userAcc.getUserByKey(collaboratorToAdd.optionalUserKey().orElseGet(null)));
								relationService.create(RelationInstance.builder(collaboratorToAdd, COLLABORATOR, content).build());
								AuthenticatedUserThreadLocal.set(currentUser);
							}
						}
					}
				}
				
				doneString += "collaborator;";
				
				if(wasTrashed) {
					ceo.trash();
				}
			}
			else {
				if(!doneString.isEmpty()) {
					anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.collaborator.permission"), (ceo.getTitle() != null) ? ceo.getTitle() : Long.toString(ceo.getLatestVersionId())), getBaseUrl() + ceo.getUrlPath(), false);
				}
			}						
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
			AuthenticatedUserThreadLocal.set(currentUser);
		}
		
		if(!doneString.isEmpty()) {
			anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.content"),
				ceo.getTypeEnum().name(), doneString, (ceo.getTitle() != null) ? ceo.getTitle() : Long.toString(ceo.getLatestVersionId())),
				String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.version"),
				ceo.getVersion(), formatDateISO8601(ceo.getLastModificationDate())),
				getBaseUrl() + ceo.getUrlPath(), success);
		}
		
		return !doneString.isEmpty();
	}
	
	
	// anonymize creator and/or last modifier of the given ContentEnitityObject
	private void anonymizeSpace(SimpleAnonymizerContext anonymizeContext, Space space) throws Exception {
		
		boolean success = true;
		String doneString = "";
		
		try {
			
			// creator
			if(anonymizeContext.currentUser.equals(space.getCreator()) && isCreationInTimeFrameAndSpace(space, anonymizeContext)) {
				if(anonymizeContext.execute) {
					space.setCreator(anonymizeContext.targetUser);
				}
				doneString += "creator;";
			}
			
			// last modifier
			if(anonymizeContext.currentUser.equals(space.getLastModifier()) && isModificationInTimeFrameAndSpace(space, anonymizeContext)) {
				if(anonymizeContext.execute) {
					space.setLastModifier(anonymizeContext.targetUser);
				}
				doneString += "modifier;";
			}
			
			// description creator
			if(space.getDescription() != null) {
				if(anonymizeContext.currentUser.equals(space.getDescription().getCreator()) && isCreationInTimeFrameAndSpace(space.getDescription(), anonymizeContext)) {
					if(anonymizeContext.execute) {
						space.getDescription().setCreator(anonymizeContext.targetUser);
					}
					doneString += "creator_desc;";
				}
				
				// description last modifier 
				if(anonymizeContext.currentUser.equals(space.getDescription().getLastModifier()) && isModificationInTimeFrameAndSpace(space.getDescription(), anonymizeContext)) {
					if(anonymizeContext.execute) {
						space.getDescription().setLastModifier(anonymizeContext.targetUser);
					}
					doneString += "modifier_desc;";
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			success = false;
		}
		
		if(!doneString.isEmpty()) {
			anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.space"), doneString, space.getKey(), formatDateISO8601(space.getLastModificationDate())), getBaseUrl() + space.getUrlPath(), success);
		}
	}

	
	// anonymize mentions in the given ContentEnitityObject	
	private void anonymizeMention(SimpleAnonymizerContext anonymizeContext, ContentEntityObject ceo) throws Exception {
		if(ceo.getBodyAsString().contains(anonymizeContext.currentUser.getKey().toString()) && isModificationInTimeFrameAndSpace(ceo, anonymizeContext)) {
			if(anonymizeContext.execute) {
				String body = ceo.getBodyAsString();
				body = replaceIfFollowedByEvenOddNumberOfOccurrences(body, anonymizeContext.currentUser.getKey().toString(), anonymizeContext.targetUser.getKey().toString(), "ac:task>", false);
				body = body.replace("<ac:plain-text-link-body><![CDATA[" + anonymizeContext.currentUser.getFullName() + "]]></ac:plain-text-link-body>", "");
				ceo.setBodyAsString(body);
			}
			anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.mention"),
				ceo.getTypeEnum().name(), (ceo.getTitle() != null) ? ceo.getTitle() : Long.toString(ceo.getLatestVersionId())),
				String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.version"),
				ceo.getVersion(), formatDateISO8601(ceo.getLastModificationDate())), getBaseUrl() + ceo.getUrlPath(), true);
		}
	}
	
	
	// anonymize tasks in the given ContentEnitityObject	
	private void anonymizeTask(SimpleAnonymizerContext anonymizeContext, ContentEntityObject ceo, boolean post) throws Exception {
		if(ceo.getBodyAsString().contains(anonymizeContext.currentUser.getKey().toString()) && isModificationInTimeFrameAndSpace(ceo, anonymizeContext)) {
			if(anonymizeContext.execute) {
				ContentEntityObject originalCeo = null;
				if(ceo.isLatestVersion()) { originalCeo = (ContentEntityObject) ceo.clone(); }
				String body = ceo.getBodyAsString();
				body = replaceIfFollowedByEvenOddNumberOfOccurrences(ceo.getBodyAsString(), anonymizeContext.currentUser.getKey().toString(), anonymizeContext.targetUser.getKey().toString(), "ac:task>", true);
				body = body.replace("<ac:plain-text-link-body><![CDATA[" + anonymizeContext.currentUser.getFullName() + "]]></ac:plain-text-link-body>", "");
				ceo.setBodyAsString(body);
				if(ceo.isLatestVersion() && post) { postUpdate(ceo, originalCeo); }
			}
			anonymizeContext.appendResult(String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.task"),
				ceo.getTypeEnum().name(), (ceo.getTitle() != null) ? ceo.getTitle() : Long.toString(ceo.getLatestVersionId())),
				String.format(i18n.getText("de.edrup.confluence.plugins.simple-anonymizer.message.version"),
				ceo.getVersion(), formatDateISO8601(ceo.getLastModificationDate())), getBaseUrl() + ceo.getUrlPath(), true);
		}
	}
	
	
	// publish an update event
	private void postUpdate(ContentEntityObject ceo, ContentEntityObject originalCeo) {
		if(ceo instanceof Page) {
			eventPublisher.publish(new PageUpdateEvent(this, (Page) ceo, (Page) originalCeo, true, PageUpdateTrigger.UNKNOWN));
		}
		if(ceo instanceof BlogPost) {
			eventPublisher.publish(new BlogPostUpdateEvent(this, (BlogPost) ceo, (BlogPost) originalCeo, true, PageUpdateTrigger.UNKNOWN));			
		}
	}
	
	
	// return the last activity date of a question in ISO format
	private String formatDateISO8601(Date d) {
	  DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	  return df.format(d);
	}
	
	
	// check whether the last change of the given ContentEntityObject is within the given time frame
	private boolean isModificationInTimeFrameAndSpace(EntityObject ceo, SimpleAnonymizerContext anonymizeContext) {
		return (ceo.getLastModificationDate().after(anonymizeContext.fromDate) && ceo.getLastModificationDate().before(anonymizeContext.toDate) && isSpaceIncluded(ceo, anonymizeContext));
	}

	
	// check whether the last change of the given ContentEntityObject is within the given time frame
	private boolean isCreationInTimeFrameAndSpace(EntityObject ceo, SimpleAnonymizerContext anonymizeContext) {
		return (ceo.getCreationDate().after(anonymizeContext.fromDate) && ceo.getCreationDate().before(anonymizeContext.toDate) && isSpaceIncluded(ceo, anonymizeContext));
	}
	
	
	// check whether the ceo belongs to a space we want to anonymize in
	private boolean isSpaceIncluded(EntityObject ceo, SimpleAnonymizerContext anonymizeContext) {
		if(ceo instanceof SpaceContentEntityObject) {
			return anonymizeContext.limitToSpaces.isEmpty() ? true : anonymizeContext.limitToSpaces.contains(((SpaceContentEntityObject) contentEntityMan.getById(((ContentEntityObject) ceo).getLatestVersionId())).getSpaceKey());
		}
		else if(ceo instanceof Comment) {
			return anonymizeContext.limitToSpaces.isEmpty() ? true : anonymizeContext.limitToSpaces.contains(((Comment) contentEntityMan.getById(((ContentEntityObject) ceo).getLatestVersionId())).getSpace().getKey());
		}
		else if(ceo instanceof Space) {
			return anonymizeContext.limitToSpaces.isEmpty() ? true : anonymizeContext.limitToSpaces.contains(((Space) ceo).getKey());
		}
		else {
			return true;
		}
	}
	
	
	// get the baseUrl
	private String getBaseUrl() {
		return settingsMan.getGlobalSettings().getBaseUrl();
	}	


	// get the detailed stack trace
	private String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
	
	
	// create random string
	private String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	
	
	// replace all occurrences of a string in case they are followed by an even number of occurrences of another string
	private String replaceIfFollowedByEvenOddNumberOfOccurrences(String searchIn, String toReplace, String replacement, String evenPhrase, boolean odd) {
		int start = 0;
		while((start = searchIn.indexOf(toReplace, start)) > -1) {
			if(odd ^ evenNoOfOccurrencesFollowing(searchIn, evenPhrase, start)) {
				searchIn = searchIn.substring(0, start) + searchIn.substring(start).replaceFirst(toReplace, replacement);
			}
			start += replacement.length();
		}
		return searchIn;
	}
	
	
	// check whether there is an even number of occurrences of a phrase in a string starting at a given position
	private boolean evenNoOfOccurrencesFollowing(String searchIn, String phrase, int start) {
		return ((((searchIn.substring(start).length() - searchIn.substring(start).replaceAll(phrase, "").length()) / phrase.length()) % 2) == 0) ? true : false;
	}
	
	
	// register token
	public void registerToken(String token) {
		validTokens.put(token, true);
	}
	
	
	// check if token is valid
	public boolean isTokenValid(String token) {
		return validTokens.get(token) != null;
	}
}
