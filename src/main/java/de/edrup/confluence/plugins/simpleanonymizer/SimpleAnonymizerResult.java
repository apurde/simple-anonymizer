package de.edrup.confluence.plugins.simpleanonymizer;

public class SimpleAnonymizerResult {
	
	public SimpleAnonymizerResultType type;
	public long contentId = 0;
	public String spaceKey = "";
	public String userName;
	
	
	public SimpleAnonymizerResult(long contentId, String userName, SimpleAnonymizerResultType type) {
		this.contentId = contentId;
		this.spaceKey = "";
		this.type = type;
		this.userName = userName;
	}
	
	
	public SimpleAnonymizerResult(String spaceKey, String userName) {
		this.spaceKey = spaceKey;
		this.contentId = 0;
		this.type = SimpleAnonymizerResultType.CONTRIBUTION_SPACE;
		this.userName = userName;
	}
}
