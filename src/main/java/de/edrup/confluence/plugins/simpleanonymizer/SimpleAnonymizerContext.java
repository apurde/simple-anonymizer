package de.edrup.confluence.plugins.simpleanonymizer;

import java.util.ArrayList;
import java.util.Date;

import com.atlassian.confluence.user.ConfluenceUser;
import org.json.JSONObject;

public class SimpleAnonymizerContext {
	
	public String userOrGroup;
	public String targetUserName;
	public String fromDateString;
	public String toDateString;
	public String limitToSpaces;
	public boolean contributions;
	public boolean likes;
	public boolean watches;
	public boolean mentions;
	public boolean tasks;
	public boolean personalSpace;
	public boolean includeArchivedSpaces;
	public boolean includeTrash;
	public boolean execute;
	public boolean limitLog;
	
	public Date fromDate;
	public Date toDate;
	
	public ArrayList<JSONObject> results;
	private int lastHash;
	
	public boolean finished;
	public float progress;
	public boolean paidLicense;
	public boolean checkLicense;
	
	public ConfluenceUser currentUser, targetUser;
	public String currentUserName;
	
	public final String MIN_DATE = "1980-01-01";
	public final String MAX_DATE = "2100-01-01";
	
	public String requestId;
	
	
	public SimpleAnonymizerContext(String userOrGroup, String targetUserName, String fromDateString, String toDateString, String limitToSpaces,
		boolean contributions, boolean likes, boolean watches, boolean mentions, boolean tasks, boolean personalSpace,
		boolean includeArchivedSpaces, boolean includeTrash, boolean execute, boolean limitLog) {
		
		this.userOrGroup = userOrGroup;
		this.targetUserName = targetUserName;
		this.fromDateString = fromDateString.isEmpty() ? MIN_DATE : fromDateString;
		this.toDateString = toDateString.isEmpty() ? MAX_DATE : toDateString;
		this.contributions = contributions;
		this.likes = likes;
		this.watches = watches;
		this.mentions = mentions;
		this.tasks = tasks;
		this.personalSpace = personalSpace;
		this.includeArchivedSpaces = includeArchivedSpaces;
		this.includeTrash = includeTrash;
		this.execute = execute;
		this.finished = false;
		this.progress = 0.0f;
		this.paidLicense = false;
		this.checkLicense = false;
		this.limitToSpaces = limitToSpaces;
		this.limitLog = limitLog;
		
		currentUserName = "";
		lastHash = 0;
		requestId = "";
		
		results = new ArrayList<JSONObject>();
	}
	

	// append a result entry to our list
	public void appendResult(String text, String url, boolean success) {
		JSONObject result = new JSONObject();
		result.put("text", text);
		result.put("success", success);
		result.put("url", url);
		results.add(result);
	}
	
	
	// append a result entry to our list considering limitLog
	public void appendResult(String text, String versionAppendix, String url, boolean success) {
		if(limitLog && (text.hashCode() != lastHash)) {
			appendResult(text.concat(versionAppendix), url, success);
			lastHash = text.hashCode();
		}
		if(!limitLog) {
			appendResult(text.concat(versionAppendix), url, success);
		}
	}
}
