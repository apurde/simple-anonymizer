package de.edrup.confluence.plugins.simpleanonymizer;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Path("/")
public class SimpleAnonymizerRest {
	
	private final SimpleAnonymizerHelper simpleAnonymizerHelper;
	private final PermissionManager permissionMan;

	
	@Inject
	public SimpleAnonymizerRest(SimpleAnonymizerHelper simpleAnonymizerHelper, @ComponentImport PermissionManager permissionMan) {
		this.simpleAnonymizerHelper = simpleAnonymizerHelper;
		this.permissionMan = permissionMan;
	}
	
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/anonymize")
    public Response anonymize(@QueryParam("userName") String userName,
    	@QueryParam("targetUserName") String targetUserName,
    	@DefaultValue("") @QueryParam("fromDate") String fromDate,
    	@DefaultValue("") @QueryParam("toDate") String toDate,
    	@DefaultValue("") @QueryParam("limitToSpaces") String limitToSpaces,
    	@DefaultValue("true") @QueryParam("contributions") boolean contributions,
       	@DefaultValue("false") @QueryParam("likes") boolean likes,
       	@DefaultValue("false") @QueryParam("watches") boolean watches,
       	@DefaultValue("false") @QueryParam("mentions") boolean mentions,
     	@DefaultValue("false") @QueryParam("tasks") boolean tasks,
       	@DefaultValue("false") @QueryParam("personalSpace") boolean personalSpace,
       	@DefaultValue("false") @QueryParam("includeArchivedSpaces") boolean includeArchivedSpaces,
    	@DefaultValue("false") @QueryParam("includeTrash") boolean includeTrash,
    	@DefaultValue("false") @QueryParam("execute") boolean execute,
    	@DefaultValue("true") @QueryParam("limitLog") boolean limitLog,
    	@DefaultValue("") @QueryParam("token") String token) {
		
		// action only allowed for admins and if token is valid
		if(!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get()) || !simpleAnonymizerHelper.isTokenValid(token)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		SimpleAnonymizerContext context = new SimpleAnonymizerContext(userName, targetUserName, fromDate, toDate, limitToSpaces, contributions, likes, watches,
			mentions, tasks, personalSpace, includeArchivedSpaces, includeTrash, execute, limitLog);
		
		return Response.ok(simpleAnonymizerHelper.anonymize(context)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/cancelRequest")
    public Response cancelRequest(@QueryParam("requestId") String requestId) {
		return Response.ok(simpleAnonymizerHelper.cancelRequest(requestId)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getStatus")
    public Response getStatus(@QueryParam("requestId") String requestId) {
		return Response.ok(simpleAnonymizerHelper.getStatus(requestId)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	

	@GET
	@Produces({MediaType.TEXT_HTML})
	@Path("/getRenderedResult")
    public Response getRenderedResult(@QueryParam("requestId") String requestId) {
		return Response.ok(simpleAnonymizerHelper.getRenderedResult(requestId)).cacheControl(getNoStoreNoCacheControl()).build();
	}


	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getJSONResult")
    public Response getJSONResult(@QueryParam("requestId") String requestId) {
		return Response.ok(simpleAnonymizerHelper.getJSONResult(requestId)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getUsers")
    public Response getUsers(@QueryParam("query") String query) {
		return Response.ok(simpleAnonymizerHelper.getUsers(query)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
}
