package de.edrup.confluence.plugins.simpleanonymizer;

import java.util.UUID;

import com.atlassian.confluence.core.ConfluenceActionSupport;


public class SimpleAnonymizerAction extends ConfluenceActionSupport {
	
	private final SimpleAnonymizerHelper simpleAnonymizerHelper;
	
	private static final long serialVersionUID = 1L;
	
	public SimpleAnonymizerAction(SimpleAnonymizerHelper simpleAnonymizerHelper) {
		this.simpleAnonymizerHelper = simpleAnonymizerHelper;
	}
	
	public String getToken() {
		String token = UUID.randomUUID().toString();
		simpleAnonymizerHelper.registerToken(token);
		return token;
	}
}
