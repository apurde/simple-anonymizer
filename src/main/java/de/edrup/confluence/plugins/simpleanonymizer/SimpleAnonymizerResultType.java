package de.edrup.confluence.plugins.simpleanonymizer;

public enum SimpleAnonymizerResultType {
	CONTRIBUTION_CONTENT, CONTRIBUTION_SPACE, MENTION, TASK, TRASH
}
