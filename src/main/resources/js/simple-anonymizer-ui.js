function SimpleAnonymizerUI() {
	
	var requestId;
	
	// init UI
	this.init = function() {
		
		// init date pickers
		AJS.$('#fromDate').datePicker({'overrideBrowserDefault': true});
		AJS.$('#toDate').datePicker({'overrideBrowserDefault': true});
		 
		// init user selectors
		var placeholderText = AJS.I18n.getText("de.edrup.confluence.plugins.simple-anonymizer.ui.name.placeholder");
		initUserSelector(AJS.$("#userName"), placeholderText);
		initUserSelector(AJS.$("#targetUserName"), placeholderText);
		
		// init space selector
		initSpaceSelector(AJS.$("#limitToSpaces"), "");
		
		// bind buttons
		AJS.$("#simulate").click(function() { execute(false); });
		AJS.$("#execute").click(function() { AJS.dialog2("#simple-anonymizer-confirmation").show(); });
		AJS.$("#cancel-job").click(function() { cancelRequest(); });
		AJS.$("#warning-confirm").click(function() { AJS.dialog2("#simple-anonymizer-confirmation").hide(); execute(true); });
		AJS.$("#warning-cancel").click(function() { AJS.dialog2("#simple-anonymizer-confirmation").hide(); });		
		
		// first progress
		AJS.progressBars.update("#simple-anonymizer-progress", 0.0);
	};
	
	
	// init the given user selector
	var initUserSelector = function(element, placeholderText) {
		
		AJS.$(element).auiSelect2({
			
			// let the user enter three letters before we search
		    minimumInputLength: 2,
		    
		    // set the placeholder
		    placeholder: placeholderText,
		    		    
		    // let select2 load matching users
		    ajax: {
		        url: AJS.contextPath() + '/rest/simpleanonymizer/1.0/getUsers',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		            return {
		                query: term
		            };
		        },
		        // map the results to a select2 format
		        results: function (response) {
		            return {
		                results: $.map(response, function (item) {
		                    return {
		                        text: item.nameForList,
		                        id: item.name
		                    };
		                })
		            };
		        },	
		    }
		});
	};
	
	
	// init the given space selector
	var initSpaceSelector = function(element, placeholderText) {
		
		AJS.$(element).auiSelect2({
			
			// let the user enter three letters before we search
		    minimumInputLength: 2,
		    
		    // set the placeholder
		    placeholder: placeholderText,
		    
		    // multiple selection
		    multiple: true,
		    
		    // let select2 load matching spaces
		    ajax: {
		        url: AJS.contextPath() + '/rest/api/search',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		            return {
		                cql: "siteSearch ~ \"" + term + "*\" and type=\"space\"",
		                includeArchivedSpaces: true
		            };
		        },
		        // map the results to a select2 format
		        results: function (response) {
		            return {
		                results: $.map(response.results, function (item) {
		                    return {
		                        text: item.space.name + " (" + item.space.key + ")",
		                        id: item.space.key
		                    };
		                })
		            };
		        },	
		    }
		});
	};
	
	
	// execute the anonymization
	var execute = function(execute) {		
		
		// delete old results, start spinner and show progress bar
		AJS.$("#resultsContainer").html("");
		AJS.$("#simple-anonymizer-spinner").spin();
		AJS.progressBars.update("#simple-anonymizer-progress", 0.0);
		AJS.$("#simple-anonymizer-progress").show();
		AJS.$("#simple-anonymizer-cancel").show();
		
		// disable buttons
		AJS.$(".simple-anonymizer-action").prop("disabled", true);
		
		var queryData = {
			execute : execute,
			userName : AJS.$("#userName").val(),
			targetUserName : AJS.$("#targetUserName").val(),
			toDate : AJS.$("#toDate").val(),
			fromDate : AJS.$("#fromDate").val(),
			limitToSpaces : AJS.$("#limitToSpaces").val(),
			contributions : AJS.$("#contributions").prop("checked"),
			likes : AJS.$("#likes").prop("checked"),
			watches : AJS.$("#watches").prop("checked"),
			mentions : AJS.$("#mentions").prop("checked"),
			tasks : AJS.$("#tasks").prop("checked"),
			personalSpace : AJS.$("#personalSpace").prop("checked"),
			includeArchivedSpaces : AJS.$("#includeArchivedSpaces").prop("checked"),
			includeTrash : AJS.$("#includeTrash").prop("checked"),
			limitLog : AJS.$("#limitLog").prop("checked"),
			token : AJS.$("#simple-anonymizer-token").text()
		};
		
		// perform request
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/simpleanonymizer/1.0/anonymize?" + AJS.$.param(queryData),
			type: 'PUT',
			success: function(result) {
				setTimeout(function() {
					requestId = result.requestId;
					checkFinished();
				}, 500);
			},
			error: function(request, status, error) {
				showError();
			}
		});
	};
	
	
	// check whether the request is finished
	var checkFinished = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/simpleanonymizer/1.0/getStatus",
			type: 'GET',
			data: {
				requestId : requestId
			},
			success : function(result) {
				AJS.progressBars.update("#simple-anonymizer-progress", result.progress);
				if(result.finished) {
					loadAndShowResult(requestId);
				}
				else {
					setTimeout(function() {
						checkFinished(requestId);
					}, 1000);
				}
			},
			error : function(request, status, error) {
				showError();
			}
		});
	};
	
	
	// load and show the result
	var loadAndShowResult = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/simpleanonymizer/1.0/getRenderedResult",
			type: 'GET',
			data: {
				requestId : requestId
			},
			success : function(result) {
				actionFinished();
				AJS.$("#resultsContainer").html(result);
			},
			error : function(request, status, error) {
				showError();
			}
		});
	};
		
	
	// show an error when something went wrong
	var showError = function() {
		actionFinished();
		AJS.$("#resultsContainer").html("Something went wrong :-(");
	};
	

	// to do when an action is finished
	var actionFinished = function() {
		AJS.$("#simple-anonymizer-spinner").spinStop();
		AJS.$("#simple-anonymizer-progress").hide();
		AJS.$("#simple-anonymizer-cancel").hide();
		AJS.progressBars.update("#simple-anonymizer-progress", 0.0);
		AJS.$(".simple-anonymizer-action").prop("disabled", false);		
	};
	
	
	// cancel a running job
	var cancelRequest = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/simpleanonymizer/1.0/cancelRequest",
			type: 'GET',
			data: {
				requestId : requestId
			}
		});
	};
}


var simpleAnonymizerUI = new SimpleAnonymizerUI();

AJS.toInit(function() {
	simpleAnonymizerUI.init();
});